---
layout: post
title: "Released first beta 2.0.0.0"
date: 2019-10-09 00:00:00
categories: updates
---

I just released the first beta version of iLyricsGrabber 2.0. 

This version has been completely rewritten to be multiplatform, but for now I released it only for Windows for reasons described in the About section of the application. iLyricsGrabber uses a new search engine, that seems to work better than before (if you want to propose other search engines comment this post or use the Bugs and suggestions section). 

I must admit that I do not have much time to dedicate to this project but since there are about 200 users per month I do not want it to be dead, for this reason, I am releasing just a few updates per year, moreover I also left iTunes for [Beets](https://beets.io/), so I am only developing this software for improving my skills and for my faithful users who also donated me.

Now just try this new version and report any bug using the [Bugs](/bugs) section, I will try to answer and to fix them when I can.

Happy downloading lyrics!

P.S. You can download the application [here](/download) and delete the old one since I am not maintaining it anymore!