---
layout: post
title: "Auto-tag your music with MusicBrainz Picard"
date: 2019-04-23 10:00:00
categories: tips
---

One down side of searching lyrics is that your music has to be perfectly tagged, mainly Title and Artist must be correct and not mispelled. It could be annoying and difficult tagging your entire library track by track, so you could try [MusicBrainz Picard](https://picard.musicbrainz.org/).

![Picard Demonstration](/assets/imgs/2019-04-20-auto-tag-your-music/picard-demonstration.gif)

Once you installed *Picard*, with these steps you can make your tracks ready for downloading lyrics (see the attached gif for the video sequence):
1. Select tracks on iTunes a drag them in the left column of *MusicBrainz Picard*
2. Click on **Scan** for scanning the tracks and searching for their tags
3. On the right column you can drag songs to correct albums
4. When done click **Save**
5. Now you should play your songs in iTunes or try to see their metadata in order to get iTunes to update the tags

Now you are ready for downloading lyrics with iTunesLyricsGrabber!