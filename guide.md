---
layout: page
title: Guide
permalink: /guide/
weight: 30
---

<style>
.center {
    text-align: center
}

/* Changelog table */
.beta {
    background-color: #89D2FE;
}

#changelog_table td, #changelog_table th {
    padding: 10px;
}

/* Guide table */
.yellow-bg {
    background-color: rgba(255, 255, 0, 255);
}

.darkorange-bg {
    background-color: rgba(255, 140, 0, 255);
}

.palegreen-bg {
    background-color: rgba(152, 251, 152, 255);
}

.greenyellow-bg {
    background-color: rgba(173, 255, 47, 255);
}

.orangered-bg {
    background-color: rgba(255, 69, 0, 255);
}

.darkred-bg {
    background-color: rgba(139, 0, 0, 255);
    color: #FFF;
}

.indianred-bg {
    background-color: rgba(205, 92, 92, 255);
}

.palevioletred-bg {
    background-color: rgba(219, 112, 147, 255);
}

.dimgrey-bg {
    background-color: rgba(105, 105, 105, 255);
    color: #FFF;
}
</style>

## Quick start

1. Select tracks you want to add lyrics

![Step 1]({{ site.baseurl }}/assets/imgs/step1-ituneslyricsgrabber.png)

2. Click on Grab Lyrics orange button to start downloading

![Step 2]({{ site.baseurl }}/assets/imgs/step2-ituneslyricsgrabber.png)

## Tips

- [[GUIDE](/tips/2019/04/23/auto-tag-your-music.html)] iTunesLyricsGrabber requires that title and artist are correct, read this guide to update them automatically

## Status list
Main table is updated with a colored status for every track you selected. See below how these status codes are explained.

<table id="changelog_table" class="table">
    <thead>
    <tr>
        <th>Status</th>
        <th>Meaning</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td class="yellow-bg">Seeking...</td>
        <td>The app is looking for the lyrics on the web. Track is valid and can be processed.</td>
    </tr>
    <tr>
        <td class="darkorange-bg">Skipped.</td>
        <td>The track has been skipped because it already has lyrics.</td>
    </tr>
    <tr>
        <td class="palegreen-bg">Lyrics found. Manipulating..</td>
        <td>Lyrics has been found, now they are manipulated and sent to iTunes.</td>
    </tr>
    <tr>
        <td class="greenyellow-bg">Completed.</td>
        <td>Lyrics has been found and added to the track, process for the track is terminated.</td>
    </tr>
    <tr>
        <td class="orangered-bg">Lyrics not found.</td>
        <td>Lyrics has not been found for the track, process is terminated.</td>
    </tr>
    <tr>
        <td class="darkred-bg">Web request timed out.</td>
        <td>The remote server didn't answer to request within the time specified in Setting » Lyrics Searching.
            Your connection is too slow, try to increase the value in Settings.
        </td>
    </tr>
    <tr>
        <td class="darkred-bg">Web request error.</td>
        <td>The app couldn't start a web request correctly, please go to <a href="https://goo.gl/forms/MwPpU1mmbVtKusFI2">Bugs&amp;Suggestions</a>
            section and send to developer
        </td>
    </tr>
    <tr>
        <td class="darkred-bg">iTunes interface error.</td>
        <td>The iTunes COM interface throws an exception, iTunes refused to add lyrics, please check if that track is read only by editing its metadata. If yes:
            <ol>
                <li>Right click on the song on iTunes</li>
                <li>Click "Open in Windows Explorer"</li>
                <li>Right click on the song file and "Properties"</li>
                <li>At the bottom of the windows that will appear, uncheck "Read Only"</li>
            </ol>
        </td>
    </tr>
    <tr>
        <td class="indianred-bg">Track error.</td>
        <td>Selected track is not a valid track, it has not lyrics field or maybe it's only on iCloud and you have to
            download it to add lyrics.
        </td>
    </tr>
    <tr>
        <td class="palevioletred-bg">Generic error when sending lyrics to iTunes.</td>
        <td>iTunes refused to add lyrics to the selected track, maybe it was not ready to do it. Please close iTunes and
            retry.
        </td>
    </tr>
    <tr>
        <td class="dimgrey-bg">Aborted by the user.</td>
        <td>User clicked on Abort button. Everything is going to stop.</td>
    </tr>
    </tbody>
</table>

## Like the app on Facebook

**<span style="color:red">I do not own the Facebook page anymore, please do not like it. I'm trying to create a subreddit con reddit.com.</span>**

To receive all updates and suggesting it to your friends like the app of facebook! Thank you!
<div class="center">
    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fituneslyricsgrabber&amp;tabs&amp;width=500&amp;height=154&amp;small_header=true&amp;adapt_container_width=true&amp;hide_cover=true&amp;show_facepile=true&amp;appId=123125311125208" width="500" height="154" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true"></iframe>
</div>