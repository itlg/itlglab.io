---
layout: page
title: Donate
permalink: /donate/
weight: 20
---

If you use a lot the application and want to support me for continuing development you can:

- <a href="https://paypal.me/gabrielepmattia">Donate</a> by using PayPal;
- <a href="https://liberapay.com/gabrielepmattia">Support me</a> by using LiberaPay;
- <a href="https://www.patreon.com/bePatron?u=34214483">Support me</a> on Patreon, you can see my page <a href="https://www.patreon.com/gabrielepmattia">here</a>. 

Whoever supports me periodically will have also priority support and the pride to support the application and me ;)

Your support is **very appreciated** and it will push myself to improve the project. I really thank everyone that donated so far. Really thank you.

If you donate, let me know if you want to appear in the list of donors, here and in the application by writing a message with your donation or sending me an email.

# Donors

_The list will be updated soon, hopefully! If you donated for older versions please [write to me](incoming+itlg-releases-11916138-issue-@incoming.gitlab.com) and I will add your name asap!_
