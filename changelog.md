---
layout: page
title: Changelog
permalink: /changelog/
weight: 40
---

<div align="center">
        <table class="table">
            <tbody><tr style="font-weight:bold">
                <td>Version</td>
                <td>Description</td>
                <td>Date</td>
            </tr>
            <tr class="beta">
                <td>2.0.0.0b</td>
                <td>- First brand new version, completely rewritten from scratch<br />
                - Natively multiplatform architecture Desktop App + Bridge
                - Brand new design, no more blurry with .NET framework
                - New lyrics search engine 
                </td>
                <td>09/10/2020</td>
            </tr>
             <tr>
                <td>1.7.1.2</td>
                <td>- Fixed SSL error on Windows 7
                </td>
                <td>27/05/2019</td>
            </tr>
            <tr>
                <td>1.7.1.1</td>
                <td>- Improved logging of web request error<br />
                    - Added windows version in log
                </td>
                <td>25/04/2019</td>
            </tr>
            <tr>
                <td>1.7.1.0</td>
                <td>- Changed lyrics source link<br />
                    - Added anonymous usage data collect<br />
                    - Added autocheck for updates<br />
                    - Some ui improvements
                </td>
                <td>18/04/2019</td>
            </tr>
            <tr>
                <td>1.7.0.1</td>
                <td>- Fixed ProgressBar value bug and Abort button bug<br>
					- Improved stability when searching for lots of tracks
                </td>
                <td>09/10/2016</td>
            </tr>            
            <tr>
                <td>1.7.0</td>
                <td>- App is now fully resizable and maximizable (even the table)<br>
                    - Added abort state for songs and improved Abort button behavior<br>
                    - Added table filter<br>
                    - Added ID3TagForcer tool (use at your own risk!)<br>
                    - Fixed Send To Developer button in Credits
                </td>
                <td>29/09/2016</td>
            </tr>
            <tr>
                <td>1.6.7</td>
                <td>- Fixed bug reporting link in Credits form<br>
                    - Moved logfile location to Windows Temp folder, before the app crashed for lack of log file
                    permissions<br>
                    - Added new kind of error when sending lyrics to iTunes, app sometimes crashed if iTunes was not
                    ready
                </td>
                <td>29/09/2016</td>
            </tr>
            <tr>
                <td>1.6.6</td>
                <td>- Fixed crashing bug when tracks were not downloaded<br>
                    -- That kind of tracks will result as "Track error"<br>
                    - Updated bug report link on Credits form<br>
                    - Internal improvements
                </td>
                <td>13/09/2016</td>
            </tr>
            <tr>
                <td>1.6.5.1</td>
                <td>- Fixed a bug that caused the app to crash when lyrics were not found</td>
                <td>09/09/2016</td>
            </tr>
            <tr>
                <td>1.6.5</td>
                <td>- Fixed crashing bug when tracks were not downloaded (issue <a href="https://bitbucket.org/gabry3795/ituneslyricsgrabber/issues/1/gpf-when-file-is-not-downloaded" target="_blank">#1</a>)<br>
                    - Added other counters<br>
                    - Some internal improvement
                </td>
                <td>09/09/2016</td>
            </tr>
            <tr>
                <td>1.6.1</td>
                <td>- Fixed crash when searching lot of song<br>
                    - Fixed bug that didn't allow to download some lyrics</td>
                <td>06/04/2016</td>
            </tr>
            <tr>
                <td>1.6.0</td>
                <td>- Fixed a bug that enabled button before every song was finished<br>
                    - Added timeout state for songs in table<br>
                    - Fixed a bug that might cause the app crashing when lots of timeout issued<br>
                    - Main form GUI changes<br>
                    - Removed abort button and integrated with Grab button<br>
                    - Added logging setting
                </td>
                <td>01/04/2016</td>
            </tr>
            <tr>
                <td>1.5.0</td>
                <td>- Parallel searching: search more than one song at time!<br>
                    - Removed useless code and UI elements<br>
                    - Added Settings form<br>
                    - Added welcome form<br>
                    - Colored table row with response<br>
                    - New design
                </td>
                <td>29/03/2016</td>
            </tr>
            <tr>
                <td>1.0.0</td>
                <td> - New engine, more efficient and faster<br>
                    - New design<br>
                    - New splash screen beahvior
                </td><td>19/06/2015</td>
            </tr>
            <tr class="beta">
                <td>0.9.8</td>
                <td>- Added "Track Count" tool to count and list selected tracks without: lyrics, album pic or album
                    name<br>
                    - New table design
                </td>
                <td>07/09/2014</td>
            </tr>
            <tr class="beta">
                <td>0.9.7</td>
                <td>- Added Splash screen checking internet connection and itunes process<br>
                    - Added "abort" button if the lyric's request takes more than 15 sec
                </td>
                <td>03/09/2014</td>
            </tr>
            <tr class="beta">
                <td>0.9.6.1</td>
                <td>- Fixed "MicrosoftReportViewer" Assembly request</td>
                <td>02/09/2014</td>
            </tr>
            <tr class="beta">
                <td>0.9.6</td>
                <td>- Added tool for removing string from tracks
                </td>
                <td>01/09/2014</td>
            </tr>
            <tr class="beta">
                <td>0.9.5</td>
                <td>- Better UI (buttons, font, table)<br>
                    - New log system (app generates detailed log.txt)<br>
                    - Working status bar<br>
                    - Overwrite button on the main screen<br>
                    - General bugfixes<br>
                    - New "about" form with possibility to open/clear log file
                </td>
                <td>30/08/2014</td>
            </tr>
            <tr class="beta">
                <td>0.9.0</td>
                <td>- BIG Update<br>
                    - Fixed the bug when selecting &gt;1 tracks<br>
                    - New design, new UI<br>
                    - Added settings page with opportunity to overwrite lyrics if alredy in the track<br>
                    - Added counter for total analyzed tracks<br>
                    - New lyrics search engine
                </td>
                <td>29/08/2014</td>
            </tr>
            <tr class="beta">
                <td>0.2.0</td>
                <td>- Created and added app icon<br>
                </td>
                <td>26/08/2014</td>
            </tr>
            <tr class="beta">
                <td>0.1.5</td>
                <td>- First beta release to public<br>
                </td>
                <td>25/08/2014
        </td></tr></tbody></table>
    </div>