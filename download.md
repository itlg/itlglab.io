---
layout: page
title: Download
permalink: /download/
description: "Download iTunes Lyrics Grabber from here immediately!"
weight: 50
---

iTunesLyricsGrabber is available as a beta release. This new beta version has been completely rewritten to be multiplatform, but for now is only available for Windows. 

My releases are 100% virus free, 100% ads free, just try and see. Go to Guide section to learn how use the software after downloading and installing it.

## Beta releases

Beta releases can be unstable and unreliable, use at your own risk and always have a backup of your iTunes library.

| Version | Checksums                                       | Ext   | Links                              |
| ------- | ----------------------------------------------- | ----- | ---------------------------------- |
| 2.0.0.0b | SHA1 `62bf1574d0e782ab38f8add38cead389508fb88b` | `msi` | {% include download_link.html url="https://gitlab.com/itlg/releases/-/raw/master/win/2_0_0_0b/iLyricsGrabber2.0.0.msi?inline=false" version="2_0_0_0" %} |

## Stable releases

No currently stable releases, but you can download old releases [in this repository](https://gitlab.com/itlg/releases/-/tree/master/win).

See how to report issues in the [Bugs](/bugs) section.

**If you found this app useful, please donate to support the development!**
