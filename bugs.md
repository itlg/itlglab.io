---
layout: page
title: Bugs
permalink: /bugs/
weight: 60
---

You can report problems or propose features by sending an email to the [Support Bot](mailto:incoming+itlg-releases-11916138-issue-@incoming.gitlab.com).

## Reporting bugs

If it is a crash describe the steps for reproducing the crash. Also write in the email your Windows, iTunes and iLyricsGrabber versions. <span style="color:red">Keep in mind that support is given only on application versions listed in the [Download](/download) section</span>!

Thank you for your help!
