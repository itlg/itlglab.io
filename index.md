---
# You don't need to edit this file, it's empty on purpose.
# Edit theme's home layout instead if you wanna make some changes
# See: https://jekyllrb.com/docs/themes/#overriding-theme-defaults
layout: page
title: Home
exclude: true
comments: false
---

<style>
.yt-embedded-container {
    position: relative;
    width: 100%;
    height: 0;
    padding-bottom: 56.25%;
}
.yt-video {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}
</style>

# Start downloading your lyrics with a click!

iLyricsGrabber is a simple free software for Windows which allows you to add automatically lyrics to your iTunes Library tracks.

Just select tracks(s) on iTunes and click on "Grab Lyrics!"

<p>
<div class="yt-embedded-container"><iframe src="//www.youtube.com/embed/pdouraDcMd0" frameborder="0" allowfullscreen class="yt-video"></iframe></div>
</p>

# News

<ul class="post-list">
    {%- assign date_format = site.minima.date_format | default: "%b %-d, %Y" -%}
    {%- for post in site.posts limit:5 -%}
    <li>
        <span class="post-meta">{{ post.date | date: date_format }}</span>
        <h3>
            <a class="post-link" href="{{ post.url | relative_url }}">
            {{ post.title | escape }}
            </a>
        </h3>
        {%- if site.show_excerpts -%}
        {{ post.excerpt }}
        {%- endif -%}
    </li>
    {%- endfor -%}
</ul>
